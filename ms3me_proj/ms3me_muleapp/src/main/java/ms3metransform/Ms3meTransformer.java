package ms3metransform;

import java.util.ArrayList;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


public class Ms3meTransformer extends AbstractMessageTransformer{
	
	int THREE = 3;
	int SEVEN = 7;
	int TWENTYONE = 21;
	String MULTOFSEVEN = "MS3";
	String MULTOFTHREE = "ME";
	String MULTOFTWENTYONE = "MS3 and ME";
	
	
	@Override
	public ArrayList<String> transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		// TODO Auto-generated method stub
		int start = Integer.parseInt((String)message.getInvocationProperty("start"));
		int end = Integer.parseInt((String)message.getInvocationProperty("end"));
		
		ArrayList<String> transformedPayload = new ArrayList<String>();
		
		for(int i = start; i <= end; i++){
			if(i % TWENTYONE == 0)
				transformedPayload.add(MULTOFTWENTYONE);
			else if(i % SEVEN == 0)
				transformedPayload.add(MULTOFSEVEN);
			else if(i % THREE == 0)
				transformedPayload.add(MULTOFTHREE);
			else
				transformedPayload.add(new Integer(i).toString());
		}
		
		return transformedPayload;
	}

}
