# ms3me_challenge

## Author
Whitney Ford 

## Problem
A client has requested a web application that calls an API-enabled backend utilizing an algorithm.  
The backend algorithm takes in 2 integers representing a range, and converts each number in the range. Multiples of 7 convert to “MS3”, multiples of 3 convert to “ME”, and multiples of both 7 and 3 convert to “MS3 and ME”.  The algorithm should be created in the most optimized way possible. The input range has a limit from a minimum of 1 to a maximum of 200. Ideally, users should be able to query inputs from either web UI or HTTP API calls.  Responses should be in the form of JSON and an HTML representation.

## Software 
Mule 3.8.3 EE

## How do I get set up?
Clone or download zip for repository.  Run the ms3me_webapp.html on app server.  
You can use live-server or an web development server to run javascript-based webapp.  If not imported through Anypoint Studio import project and run.  Web app and mule backend should be running at the same time
